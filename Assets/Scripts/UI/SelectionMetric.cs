﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class SelectionMetric : MonoBehaviour
{
    [Inject] SelectionManager _SelectionManager;
    [SerializeField] Text text;

    public void Initialize(bool to)
    {
        if (to)
            _SelectionManager.onSelect += UpdateText;
        else
            _SelectionManager.onSelect -= UpdateText;
    }

    public void UpdateText()
    {
        int count = _SelectionManager.Selected.Count;
        text.text = $" {count} item" + ((count > 1) ? "s " : " ") + "selected";
    }


}
