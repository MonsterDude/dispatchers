﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class SelectionManager : MonoBehaviour
{

    [Inject] InputManager _InputManager;
    [Inject] UImanager _UImanager;

    public List<IFieldable> Selected => new List<IFieldable>(selected);
    List<IFieldable> selected { get { return _selected; } set { _selected = value; _UImanager.selectionmetric.UpdateText(); } }
    [SerializeField] List<IFieldable> _selected = new List<IFieldable>();
    public System.Action onSelect;

    public void Initialize(bool to)
    {
        if (to)
        { 
            _InputManager.onClick0BeginMiss += () => Select(null, false, false);
        }
        else
        {
            _InputManager.onClick0BeginMiss -= () => Select(null, false, false);
        }
    }

    public void ProcessSelection(IFieldable toSelect)
    {
        Select(toSelect, !selected.Contains(toSelect), _InputManager.shiftActive);
        onSelect?.Invoke();
    }

    void Select(IFieldable toSelect, bool state, bool additive)
    {
        //Clear by click on empty space
        if (toSelect == null)
        {
            DeselectAll();
        }
        else
        {
            if (additive)
            {
                if (state)
                    ConsumateSelection();
                else
                    ConsumateSelection();
            }
            else
            {
                if (selected.Count > 0 || !state)
                    DeselectAll(toSelect);
                if (state)
                    ConsumateSelection();
            }
        }

        void ConsumateSelection()
        {
            if (state)
                selected.Add(toSelect);
            else
                selected.Remove(toSelect);
            toSelect.ShowSelection(state);
            _UImanager.selectionmetric.UpdateText();
        }
    }

    public void DeselectAll(IFieldable except = null)
    {
        for (int i = selected.Count - 1; i > -1; i--)
        {
            if (except == null || selected[i] != except)
            {
                selected[i].ShowSelection(false);
                selected.RemoveAt(i);
            }
            else
                continue;
        }
        _UImanager.selectionmetric.UpdateText();
    }

}
