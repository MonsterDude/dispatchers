﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using static SerializationManager;

public class FieldManager : MonoBehaviour
{
    [Inject] Prefab_Refs _PF_refs;
    [Inject] UImanager _UImanager;
    [Inject] InputManager _InputManager; 
    [Inject] DiContainer _Container;
    [Inject] Pool<IFieldable> _itemPool;
    [Inject] SerializationManager _SerializationManager;
    [Inject] SelectionManager _SelectionManager;

    
    public List<IFieldable> AllFieldables => new List<IFieldable>(all);
    [SerializeField] List<IFieldable> all = new List<IFieldable>();

    

    public void Initialize(bool to)
    {
        if (to)
        {
            CalculateFieldCoords(); 
        } 
    }

    public void Move(Vector3 moveBy)
    {
        if (moveBy == null) return;
        for (int i = 0; i < _SelectionManager.Selected.Count; i++)
        {
            _SelectionManager.Selected[i].Move(moveBy);
        }
    }

    

    public void Load(List<JSONfieldObject> objs)
    {
        ClearField();
        for (int i = 0; i < objs.Count; i++)
        {
            JSONfieldObject current = objs[i];
            Sprite sprite;
            _SerializationManager.allSprites.TryGetValue(current.spriteName, out sprite);
            if (sprite == null) {
                Debug.LogError($"Unknown sprite: {current.spriteName}");
                continue; 
            }
            CreateFieldItem(sprite, current.scale, current.position, current.pixelSize, current.color);
        }
    }

    

    public void DeleteSelected()
    {
        for (int i = _SelectionManager.Selected.Count-1; i >= 0; i--)
        {
            Destroy(_SelectionManager.Selected[i]);
        }
        _UImanager.selectionmetric.UpdateText();
    }

    public void ClearField()
    {
        for (int i = all.Count - 1; i > -1; i--)
        {
            Destroy(all[i]);
        }
        all = new List<IFieldable>();
        _UImanager.selectionmetric.UpdateText();
    }

    public void ChangeColor(Color c)
    {
        for (int i = 0; i < _SelectionManager.Selected.Count; i++)
        {
            _SelectionManager.Selected[i].SetColor(c);
        }
    }

    public void ChangeScale(float value)
    {
        for (int i = 0; i < _SelectionManager.Selected.Count; i++)
        {
            _SelectionManager.Selected[i].SetScale(Vector3.one*value);
        }
    } 

    public void CreateFieldItem(Sprite sprite)
    {  
        CreateFieldItem(sprite, Vector3.one, _UImanager.dragManager.GetCenteredToolbarPosition(), _UImanager.dragManager.GetToolbarSize(), Color.white); 
    }

    public IFieldable CreateFieldItem(Sprite sprite, Vector3 scale, Vector3 position, Vector3 pixelSize, Color color)
    {
        IFieldable fieldable = _itemPool.Get();
        _Container.Inject(fieldable);
        fieldable.Initialize();
        fieldable.SetParent(_UImanager.fieldPanel);
        fieldable.SetScale(scale);
        fieldable.SetPosition(position);
        fieldable.SetSprite(sprite);
        fieldable.SetPixelSize(pixelSize);
        fieldable.SetColor(color);
        all.Add(fieldable);
        return fieldable;
    }

    public void Destroy(IFieldable item)
    {
        item.ShowSelection(false);
        _itemPool.Remove(item);
    }

    void CalculateFieldCoords() {
        (Vector2,Vector2) sizes = _UImanager.CalculateFieldSize(); 
    }

}
